# urls home

from django.contrib import admin
from django.urls import path, include
from . import views

urlpatterns = [
    path('admin/', admin.site.urls),
    path('schedule', include('form.urls')),
    path('gallery', include('gallery.urls')),
    # path('about/', views.about),
    # path('education/', views.education),
    # path('contact/', views.contact),
    path('', views.index),
]
