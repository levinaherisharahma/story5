from django.db import models
from datetime import datetime
from django.core.exceptions import ValidationError

# def validate_tanggal(value):
# 	try:
# 		input_tanggal = datetime.strptime(value, '%Y/%m/%d')
# 	except ValueError:
# 		raise ValidationError('Incorrect format')

# Kalau pake form dibikin sama kaya forms.py
# Kalau pake modelform bikinnya disini
class ModelJadwal(models.Model):
	HARI = (
		('Senin', 'Senin'),
		('Selasa', 'Selasa'),
		('Rabu', 'Rabu'),
		('Kamis', 'Kamis'),
		('Jumat', 'Jumat'),
		('Sabtu', 'Sabtu'),
		('Minggu', 'Minggu'),
	)

	KATEGORI = (
		('Kelas', 'Kelas'),
		('Nugas', 'Nugas'),
		('UKOR', 'UKOR'),
		('Hangout', 'Hangout'),
		('Gabut', 'Gabut'),
	)

	hari 		= models.CharField(max_length=10, choices=HARI, default='Senin')
	tanggal		= models.DateField()
	waktu		= models.TimeField()
	kegiatan	= models.CharField(max_length=50)
	tempat		= models.CharField(max_length=50)
	kategori	= models.CharField(max_length=10, choices=KATEGORI, default='Kelas')
	