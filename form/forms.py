from django import forms
from .models import ModelJadwal
from django.forms import ModelForm, TextInput, SelectDateWidget

# Form menggunakan ModelForm
class FormJadwal(forms.ModelForm):
	class Meta:
		model = ModelJadwal
		fields = [
			'hari',
			'tanggal',
			'waktu',
			'kegiatan',
			'tempat',
			'kategori',
		]
		widgets = {
			'tanggal': TextInput(attrs={'placeholder': 'YYYY-MM-DD'}),
			'tanggal': SelectDateWidget(),
			'waktu': TextInput(attrs={'placeholder': 'HH:MM'}),
			'kegiatan': TextInput(attrs={'placeholder': 'Nonton, belajar, dll'}),
			'tempat': TextInput(attrs={'placeholder': 'Kos, Fasilkom, dll'}),
		}


# class FormJadwal(forms.Form):
# 	HARI = (
# 		('Senin', 'Senin'),
# 		('Selasa', 'Selasa'),
# 		('Rabu', 'Rabu'),
# 		('Kamis', 'Kamis'),
# 		('Jumat', 'Jumat'),
# 		('Sabtu', 'Sabtu'),
# 		('Minggu', 'Minggu'),
# 	)

# 	KATEGORI = (
# 		('Kelas', 'Kelas'),
# 		('Nugas', 'Nugas'),
# 		('UKOR', 'UKOR'),
# 		('Hangout', 'Hangout'),
# 		('Gabut', 'Gabut'),
# 	)

# 	hari		= forms.ChoiceField(choices=HARI)
# 	waktu		= forms.DateTimeField(widget=forms.TextInput(attrs={'placeholder': 'YYYY-MM-DD hh:mm'}))
# 	kegiatan	= forms.CharField(max_length=50, widget=forms.TextInput(attrs={'placeholder': 'Nonton, belajar, dll'}))
# 	tempat		= forms.CharField(max_length=50, widget=forms.TextInput(attrs={'placeholder': 'Kos, Fasilkom, dll'}))
# 	kategori	= forms.ChoiceField(choices=KATEGORI)
	